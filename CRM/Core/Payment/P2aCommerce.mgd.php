<?php

use CRM_P2aCommerce_ExtensionUtil as E;

/**
 * The record will be automatically inserted, updated, or deleted from the
 * database as appropriate. For more details, see "hook_civicrm_managed" at:
 * https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_managed/
 */
return [
  [
    'name' => '2A Commerce',
    'entity' => 'PaymentProcessorType',
    'params' => [
      'version' => 3,
      'name' => '2A Commerce',
      'title' => E::ts('2A Commerce'),
      'description' => '2A Commerce Payment Processor',
      'class_name' => 'Payment_2ACommerce',
      'user_name_label' => 'Merchant user name*',
      'signature_label' => 'Merchant API key*',
      'billing_mode' => 1,
      'payment_type' => 1,
      'is_recur' => 1,
    ],
  ],
];
