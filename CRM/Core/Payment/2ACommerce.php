<?php

use Civi\Payment\PropertyBag;
use CRM_P2aCommerce_ExtensionUtil as E;
use Civi\Payment\Exception\PaymentProcessorException;

/**
 *
 * @package CRM
 * @copyright CiviCRM LLC https://civicrm.org/licensing
 */


/**
 * Class CRM_Core_Payment_2ACommerce for 2ACommerce (Fluidpay) implementation.
 */
class CRM_Core_Payment_2ACommerce extends CRM_Core_Payment {
  use CRM_Core_Payment_RecurringHelperTrait;
  use CRM_Core_Payment_ResponseStatusTrait;
  use CRM_Core_Payment_RequestTrait;

  private string $_mode;

  /**
   * @var \GuzzleHttp\Client
   */
  protected \GuzzleHttp\Client $guzzleClient;

  /**
   * Constructor.
   *
   * @param string $mode the mode of operation: live or test
   * @param array $paymentProcessor
   *
   * @return void
   */
  function __construct(string $mode, array $paymentProcessor) {
    $this->_mode = $mode;
    $this->_paymentProcessor = $paymentProcessor;
  }

  public function getGuzzleClient($config = []): \GuzzleHttp\Client {
    return $this->guzzleClient ?? new \GuzzleHttp\Client([] + $config);
  }

  /**
   * @param \GuzzleHttp\Client $guzzleClient
   */
  public function setGuzzleClient(\GuzzleHttp\Client $guzzleClient) {
    $this->guzzleClient = $guzzleClient;
  }


  public function checkConfig(): ?string {
    $main_fields = [
      'accepted_credit_cards' => 'Accepted Credit Card values are empty.',
      'user_name' => 'Please, set at least your Merchant user name',
      'signature' => 'Live mode merchant code is not set'
    ];

    $additional_fields = [
      'test_user_name' => 'Please, set at least your Merchant user name for Test section',
      'test_signature' => 'Test mode merchant code is not set'
    ];

    $errors = [];
    foreach($main_fields as $field_name => $error_message) {
      if(empty($this->_paymentProcessor[$field_name])) {
        $errors[] = E::ts($error_message);
      }
    }

    foreach($additional_fields as $field_name => $error_message) {
      if(isset($_POST[$field_name]) && empty($_POST[$field_name])) {
        $errors[] = E::ts($error_message);
      }
    }

    return !empty($errors) ? implode('<p>', $errors) : NULL;
  }

  /**
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \Civi\Payment\Exception\PaymentProcessorException
   */
  public function doPayment(&$params, $component = 'contribute'): array {
    $propertyBag = PropertyBag::cast($params);

    if(0 == $propertyBag->getAmount()) {
      $result['payment_status_id'] = CRM_Core_PseudoConstant::getKey('CRM_Contribute_BAO_Contribution', 'contribution_status_id', 'Completed');
      $result['payment_status'] = 'Completed';
      return $result;
    }


    $returnData = [];
    $processorFormattedParams = [
      'amount' => $propertyBag->getAmount(),
      'order_id' => $propertyBag->getter('contributionID', TRUE, ''),
    ];
    if ($propertyBag->has('description')) {
      $processorFormattedParams['description'] = $propertyBag->getDescription();
    }
    CRM_Utils_Hook::alterPaymentProcessorParams($this, $propertyBag, $processorFormattedParams);
    try {
        $this->setGuzzleClient(
          $this->getGuzzleClient([
            'base_uri' => $this->_paymentProcessor['url_site'],
            'verify' => false,
            'headers' => [
              'Authorization' => $this->_paymentProcessor['signature']
            ]
          ])
        );
        $propertyBag->setCustomProperty('FPCustomerID', $this->buildCustomerID($propertyBag));
        $customer_data = $this->buildCustomerData($propertyBag);

        $customer = $this->createCustomer($customer_data);
        if('success' == $customer['status']) {
          if(!$propertyBag->getIsRecur()) {
            $transaction_data = $this->buildSingleTransactionData($propertyBag, $customer);
            $transaction = $this->processTransaction($transaction_data);
          } else {
            $transaction_data = $this->buildRecurringTransactionData($propertyBag, $customer);
            $transaction = $this->processTransaction($transaction_data);
            if('success' == $transaction['status']) {
              // How often to run the billing cycle (run every X months, days)
              $billing_cycle_interval = $this->getBillingCycleInterval(
                $propertyBag->getRecurFrequencyUnit()
              );

              // How often to run the plan within a billing cycle.
              // (monthly, yearly, daily)
              $billing_frequency = $this->getBillingFrequency(
                $propertyBag->getRecurFrequencyUnit()
              );

              // Which day of the month to bill on.
              // For the last day of the month, use '0'.
              $billing_days = $this->getBillingDays(
                $propertyBag->getRecurFrequencyUnit()
              );

              $plans = $this->getAllPlans();
              $gateway_plans = $plan = [];

              if('success' == $plans['status'])
                $gateway_plans = $plans['data'];


              foreach ($gateway_plans as $key => $gateway_plan) {
                if (
                  $gateway_plan['amount'] == $propertyBag->getAmount() * 100 &&
                  $gateway_plan['billing_cycle_interval'] == $billing_cycle_interval &&
                  $gateway_plan['billing_frequency'] == $billing_frequency
                ) {
                  $plan = $gateway_plan;
                  break;
                }
              }

              if (!isset($plan['id'])) {
                $donation_price = $propertyBag->getAmount();
                $plan_body = $this->buildPlanData($donation_price, $propertyBag, $billing_cycle_interval, $billing_frequency, $billing_days);
                $plan_response = $this->createPlan($plan_body);
                if ('success' == $plan_response['status']) {
                  $plan = $plan_response['data'];
                }
                else {
                  throw new PaymentProcessorException($transaction['msg']);
                }
              }
              $next_billing_date = $this->getNextBillingDate(
                $propertyBag->getRecurFrequencyUnit()
              );
              $subscription = $this->buildSubscriptionData($plan['id'], $customer['data'], $propertyBag, $billing_cycle_interval, $billing_frequency, $billing_days, $next_billing_date);
              $subscription_response = $this->createSubscription($subscription);
              if('success' != $subscription_response['status']) {
                civicrm_api3_create_error($subscription_response['msg']);
              }
            } else {
              civicrm_api3_create_error($transaction['msg']);
            }
          }

          if('success' == $transaction['status']) {
            $contribution_data = civicrm_api3('Contribution', 'get', [
              'sequential' => 1,
              'return' => ['contact_id'],
              'id' => $propertyBag->getContributionID(),
            ]);
            civicrm_api3('Contact', 'create', [
              'contact_type' => 'Individual',
              'id' => $contribution_data['values'][0]['contact_id'],
              'external_identifier' => $propertyBag->getter('FPCustomerID'),
            ]);
            $returnData = [
              'payment_status'    => 'Completed',
              'payment_status_id' => CRM_Core_PseudoConstant::getKey(
                'CRM_Contribute_BAO_Contribution',
                'contribution_status_id',
                'Completed'),
              'trxn_id'           => $transaction['data']['id'],
              'fee_amount'        => 0
            ];
          } else {
            throw new PaymentProcessorException($transaction['msg']);
          }
        }
    } catch (Exception $e) {
      throw new PaymentProcessorException($e->getMessage());
    }
    return $returnData;
  }

  /**
   * @throws \CiviCRM_API3_Exception
   */
  public function handlePaymentNotification() {
    $q = explode('/', $_GET['q']);
    $paymentProcessorID = array_pop($q);

    $params = array_merge($_GET, $_REQUEST);
    $data = file_get_contents('php://input');
    $params = array_merge($params, json_decode($data, true));
    if(json_last_error() == JSON_ERROR_NONE) {
      $this->_paymentProcessor = civicrm_api3('payment_processor', 'getsingle', ['id' => $paymentProcessorID]);
      $this->processPaymentNotification($params);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getPaymentFormFields(): array {
    return ['credit_card_type', 'credit_card_number', 'cvv2', 'credit_card_exp_date'];
  }

  public function supportsRefund(): bool {
    return FALSE;
  }

  /**
   * @param array $params
   *
   * @return void
   * @throws \CiviCRM_API3_Exception
   */
  private function processPaymentNotification(array $params): void {
    $transactionID = $params['data']['response_body']['card']['id'] ?? '';
    $customerID = $params['data']['customer_id'] ?? '';
    // Must have the transaction id.
    if (empty($transactionID) || empty($customerID)) {
      CRM_Utils_System::civiExit();
    }

    if('success' == $params['status']) {
      $customer_id_parts = explode('-', $customerID);
      civicrm_api3('Payment', 'create', [
        'contribution_id' => $customer_id_parts[3], // get related Contribution ID
        'total_amount' => $params['data']['amount'],
        'payment_instrument_id' => $this->_paymentProcessor['payment_instrument_id'],
        'trxn_id' => $params['data']['id'],
        'trxn_date' => date('Y-m-d H:m:s', strtotime($params['data']['created_at']))
      ]);
    }
    CRM_Utils_System::civiExit();
  }

}
