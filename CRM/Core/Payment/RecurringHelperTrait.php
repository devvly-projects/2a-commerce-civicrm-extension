<?php

trait CRM_Core_Payment_RecurringHelperTrait {

  /**
   * @param string $period
   *
   * @return string
   */
  public function getBillingFrequency(string $period = ''): string {
    return ('month' == $period || 'year' == $period) ? 'monthly' : 'daily';
  }

  /**
   * @param string $period
   *
   * @return int
   */
  public function getBillingCycleInterval(string $period = ''): int {
    // use 1 for month or day period, and 12 (months) for year
    return ('month' == $period || 'day' == $period || empty($period)) ? 1 : 12;
  }

  /**
   * @param string $period
   *
   * @return string
   */
  public function getBillingDays(string $period = ''): string {
    if('day' == $period)
      return '1';

    // use the actual day of previous payment
    if('year' == $period)
      return date('d');

    // monthly by default
    return '0';
  }


  /**
   * @param string $period
   *
   * @return string
   */
  public function getNextBillingDate(string $period = ''): string {
    if ($period == 'day') {
      return date('Y-m-d', strtotime(date('Y-m-d') . ' + 1 day'));
    }
    if ($period == 'month') {
      return date('Y-m-d', strtotime(date('Y-m-d') . ' +1 month'));
    }
    return date('Y-m-d', strtotime(date('Y-m-d') . ' +12 months'));
  }
}
