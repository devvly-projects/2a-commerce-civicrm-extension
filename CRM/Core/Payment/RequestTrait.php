<?php

use Civi\Payment\PropertyBag;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;

trait CRM_Core_Payment_RequestTrait {

  /**
   * @throws \JsonException
   */
  public function createCustomer(array $customer) {
    $data = [];

    $response = $this->getGuzzleClient()->post('/api/vault/customer', [
      RequestOptions::HEADERS => [
        'Content-Type' => 'application/json'
      ],
      'body' => json_encode($customer)
    ]);
    if (200 == $response->getStatusCode()) {
      $contents = $response->getBody()->getContents();
      $data = json_decode($contents, TRUE, 512, JSON_THROW_ON_ERROR);
    }
    return $data;
  }

  /**
   * @return array|mixed
   * @throws GuzzleException
   * @throws JsonException
   */
  public function processTransaction($transaction): mixed {
    $data = [];

    $response = $this->getGuzzleClient()->post('/api/transaction', [
      RequestOptions::HEADERS => [
        'Content-Type' => 'application/json'
      ],
      'body' => json_encode($transaction)
    ]);
    if (200 == $response->getStatusCode()) {
      $contents = $response->getBody()->getContents();
      $data = json_decode($contents, TRUE, 512, JSON_THROW_ON_ERROR);
    }
    return $data;
  }

  /**
   * @return array|mixed
   * @throws GuzzleException
   * @throws JsonException
   */
  public function getAllPlans(): mixed {
    $data = [];

    $response = $this->getGuzzleClient()->get('/api/recurring/plans');
    if (200 == $response->getStatusCode()) {
      $contents = $response->getBody()->getContents();
      $data = json_decode($contents, TRUE, 512, JSON_THROW_ON_ERROR);
    }
    return $data;

  }

  /**
   * @return array|mixed
   * @throws GuzzleException
   * @throws JsonException
   */
  public function createPlan($plan): mixed {
    $data = [];

    $response = $this->getGuzzleClient()->post('/api/recurring/plan', [
      RequestOptions::HEADERS => [
        'Content-Type' => 'application/json'
      ],
      'body' => json_encode($plan)
    ]);
    if (200 == $response->getStatusCode()) {
      $contents = $response->getBody()->getContents();
      $data = json_decode($contents, TRUE, 512, JSON_THROW_ON_ERROR);
    }
    return $data;
  }

  /**
   * @return array|mixed
   * @throws GuzzleException
   * @throws JsonException
   */
  public function createSubscription($subscription): mixed {
    $data = [];

    $response = $this->getGuzzleClient()->post('/api/recurring/subscription', [
      RequestOptions::HEADERS => [
        'Content-Type' => 'application/json'
      ],
      'body' => json_encode($subscription)
    ]);
    if (200 == $response->getStatusCode()) {
      $contents = $response->getBody()->getContents();
      $data = json_decode($contents, TRUE, 512, JSON_THROW_ON_ERROR);
    }
    return $data;
  }

  /**
   * @return array|mixed
   * @throws GuzzleException
   * @throws JsonException
   */
  public function refund($refund_data): mixed {
    $data = [];

    $response = $this->getGuzzleClient()->post("/api/transaction/{$refund_data['transaction_id']}/refund", [
      RequestOptions::HEADERS => [
        'Content-Type' => 'application/json'
      ],
      'body' => json_encode($refund_data)
    ]);
    if (200 == $response->getStatusCode()) {
      $contents = $response->getBody()->getContents();
      $data = json_decode($contents, TRUE, 512, JSON_THROW_ON_ERROR);
    }
    return $data;
  }

  /**
   * @param \Civi\Payment\PropertyBag $propertyBag
   *
   * @return array
   * @throws \Exception
   */
  private function buildCustomerData(\Civi\Payment\PropertyBag $propertyBag): array {
    $credit_card_exp_date = $propertyBag->getCustomProperty('credit_card_exp_date');
    return [
      'id' => $propertyBag->getter('FPCustomerID'),
      'flags' => ['surcharge_exempt'],
      'default_payment' => [
        'card' => [
          'number' => $propertyBag->getCustomProperty('credit_card_number'),
          'expiration_date' => str_pad($credit_card_exp_date['M'], 2, '0', STR_PAD_LEFT) . '/' . $credit_card_exp_date['Y'],
          'cvc' => $propertyBag->getCustomProperty('cvv2')
        ]
      ],
      'default_billing_address' => [
        'first_name' => $propertyBag->getFirstName(),
        'last_name' => $propertyBag->getLastName(),
        'line_1' => $propertyBag->getBillingStreetAddress(),
        'line_2' => '',
        'city' => $propertyBag->getBillingCity(),
        'state' => $propertyBag->getBillingStateProvince(),
        'postal_code' => $propertyBag->getBillingPostalCode(),
        'country' => 'US',
        'email' => $propertyBag->getEmail()
      ],
    ];
  }

  /**
   * @throws \Exception
   */
  private function buildCustomerID(PropertyBag $propertyBag): string {
    return implode('-', [
        bin2hex(random_bytes(8)),
        strtolower( trim($propertyBag->getFirstName()) ),
        strtolower( trim($propertyBag->getLastName()) ),
        $propertyBag->getContributionID()
      ]);
  }

  private function buildSingleTransactionData(\Civi\Payment\PropertyBag $propertyBag, array $customer): array {
    return [
      'type' => 'sale',
      'order_id' => $propertyBag->getContributionID(),
      'amount' => ((float) $propertyBag->getAmount()) * 100,
      'tax_amount' => 0,
      'shipping_amount' => 0,
      'currency' => 'USD',
      'ip_address' => CRM_Utils_System::ipAddress(),
      'email_receipt' => TRUE,
      'email_address' => $propertyBag->getEmail(),
      'create_vault_record' => FALSE,
      'payment_method' => [
        'customer' => [
          'id' => $customer['data']['id'],
          'payment_method_type' => 'card',
          'payment_method_id' => $customer['data']['data']['customer']['defaults']['payment_method_id'],
          'billing_address_id' => $customer['data']['data']['customer']['defaults']['billing_address_id'],
          'shipping_address_id' => $customer['data']['data']['customer']['defaults']['billing_address_id']
        ]
      ],
      'billing_address' => [
        'first_name' => $propertyBag->getFirstName(),
        'last_name' => $propertyBag->getLastName(),
        'line_1' => $propertyBag->getCustomProperty('address_name'),
        'line_2' => '',
        'city' => $propertyBag->getBillingCity(),
        'state' => $propertyBag->getBillingStateProvince(),
        'postal_code' => $propertyBag->getBillingPostalCode(),
        'country' => 'US',
        'email' => $propertyBag->getEmail()
      ]
    ];
  }

  private function buildRecurringTransactionData(\Civi\Payment\PropertyBag $propertyBag, array $customer): array {
    return [
      'type' => 'sale',
      'billing_method' => 'recurring',
      'order_id' => $propertyBag->getContributionID(),
      'amount' => ((float) $propertyBag->getAmount()) * 100,
      'tax_amount' => 0,
      'shipping_amount' => 0,
      'currency' => 'USD',
      'ip_address' => CRM_Utils_System::ipAddress(),
      'email_receipt' => FALSE,
      'email_address' => $propertyBag->getEmail(),
      'create_vault_record' => FALSE,
      'payment_method' => [
        'customer' => [
          'id' => $customer['data']['id'],
          'payment_method_type' => 'card',
          'payment_method_id' => $customer['data']['data']['customer']['defaults']['payment_method_id'],
          'billing_address_id' => $customer['data']['data']['customer']['defaults']['billing_address_id'],
          'shipping_address_id' => $customer['data']['data']['customer']['defaults']['billing_address_id']
        ]
      ],
      'billing_address' => [
        'first_name' => $propertyBag->getFirstName(),
        'last_name' => $propertyBag->getLastName(),
        'line_1' => $propertyBag->getCustomProperty('address_name'),
        'line_2' => '',
        'city' => $propertyBag->getBillingCity(),
        'state' => $propertyBag->getBillingStateProvince(),
        'postal_code' => $propertyBag->getBillingPostalCode(),
        'country' => 'US',
        'email' => $propertyBag->getEmail()
      ]
    ];
  }

  /**
   * @param mixed $donation_price
   * @param \Civi\Payment\PropertyBag $propertyBag
   * @param int $billing_cycle_interval
   * @param string $billing_frequency
   * @param string $billing_days
   *
   * @return array
   */
  private function buildPlanData(mixed $donation_price, PropertyBag $propertyBag, int $billing_cycle_interval, string $billing_frequency, string $billing_days): array {
    return [
      'name' => "Donation-$donation_price",
      'description' => 'Plan for recurring donation',
      'amount' => $propertyBag->getAmount() * 100,
      'billing_cycle_interval' => $billing_cycle_interval,
      'billing_frequency' => $billing_frequency,
      'billing_days' => $billing_days,
      'duration' => 0,
      'add_ons' => [],
      'discounts' => []
    ];
  }

  /**
   * @param $plan_id
   * @param $customer
   * @param \Civi\Payment\PropertyBag $propertyBag
   * @param int $billing_cycle_interval
   * @param string $billing_frequency
   * @param string $billing_days
   * @param string $next_billing_date
   *
   * @return array
   */
  private function buildSubscriptionData($plan_id, $customer, PropertyBag $propertyBag, int $billing_cycle_interval, string $billing_frequency, string $billing_days, string $next_billing_date): array {
    return [
      'plan_id' => $plan_id,
      'description' => 'Subscription description',
      'customer' => ['id' => $customer['id']],
      'amount' => $propertyBag->getAmount() * 100,
      'billing_cycle_interval' => $billing_cycle_interval,
      'billing_frequency' => $billing_frequency,
      'billing_days' => $billing_days,
      'duration' => 0,
      'next_bill_date' => $next_billing_date,
      'add_ons' => [],
      'discounts' => [],
      'payment_method' =>
        [
          'customer' => [
            'id' => $customer['id'],
            'payment_method_type' => 'card',
            'payment_method_id' => $customer['data']['customer']['defaults']['payment_method_id'],
            'billing_address_id' => $customer['data']['customer']['defaults']['billing_address_id'],
            'shipping_address_id' => $customer['data']['customer']['defaults']['billing_address_id'],
          ]
        ]
    ];
  }

}
