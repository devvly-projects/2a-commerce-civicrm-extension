<?php

trait CRM_Core_Payment_ResponseStatusTrait {
  protected array $responseTable = [
    '0' => [
      'definition'  => 'Unknown status',
      'description' => 'Unknown, please contact support for more information'
    ],
    '99' => [
      'definition'  => 'Pending payment',
      'description' => 'Used in redirect processors prior to payment being received'
    ],
    '100' => [
      'definition'  => 'Approved',
      'description' => 'Transaction was successfully approved'
    ],
    '110' => [
      'definition'  => 'Partial approval',
      'description' => 'Transaction was successfully approved, but for a lesser amount'
    ],
    '101' => [
      'definition'  => 'Approved, pending customer approval',
      'description' => 'Transaction is pending customer approval before release'
    ],
    '200' => [
      'definition'  => 'Decline',
      'description' => 'Generic decline with no additional information provided by the issuer'
    ],
    '201' => [
      'definition'  => 'Do not honor',
      'description' => 'Generic decline with no additional information provided by the issuer'
    ],
    '202' => [
      'definition'  => 'Insufficient funds',
      'description' => 'Declined for insufficient funds'
    ],
    '204' => [
      'definition'  => 'Invalid transaction',
      'description' => 'Declined as the issuer does not recognize the transaction'
    ],
    '205' => [
      'definition'  => 'SCA Decline',
      'description' => 'Soft Decline indicating that a SCA challenge is required'
    ],
    '220' => [
      'definition'  => 'Invalid Amount',
      'description' => 'Provided amount is not supported by the issuer'
    ],
    '221' => [
      'definition'  => 'No such Issuer',
      'description' => 'The issuing bank can not be found'
    ],
    '222' => [
      'definition'  => 'No credit Acct',
      'description' => 'Invalid credit card number'
    ],
    '223' => [
      'definition'  => 'Expired Card',
      'description' => 'Credit card as expired and can not be processed'
    ],
    '225' => [
      'definition'  => 'Invalid CVC',
      'description' => 'Invalid CVC or CVV2 value has been provided'
    ],
    '226' => [
      'definition'  => 'Cannot Verify Pin',
      'description' => 'Card requires PIN'
    ],
    '240' => [
      'definition'  => 'Refer to issuer',
      'description' => 'Generic decline by the issuing bank'
    ],
    '250' => [
      'definition'  => 'Pick up card (no fraud)',
      'description' => 'Decline where issuer is requesting the merchant hold the card'
    ],
    '251' => [
      'definition'  => 'Lost card, pick up (fraud account)',
      'description' => 'Decline where issuer is requesting the merchant hold the card'
    ],
    '252' => [
      'definition'  => 'Stolen card, pick up (fraud account)',
      'description' => 'Decline where issuer is requesting the merchant hold the card'
    ],
    '253' => [
      'definition'  => 'Pick up card, special condition',
      'description' => 'Decline where issuer is requesting the merchant hold the card'
    ],
    '261' => [
      'definition'  => 'Stop recurring',
      'description' => 'Decline requesting recurring be stopped'
    ],
    '262' => [
      'definition'  => 'Stop recurring',
      'description' => 'Decline requesting recurring be stopped'
    ],
    '300' => [
      'definition'  => 'Gateway Decline',
      'description' => 'Generic Platform decline'
    ],
    '301' => [
      'definition'  => 'Gateway Decline - Duplicate Transaction',
      'description' => 'The gateway detected this as a duplicate transaction. Order ID, payment type, amount and payment method are used to detect duplicate transactions.'
    ],
    '310' => [
      'definition'  => 'Gateway Decline - Rule Engine',
      'description' => 'Platform has declined based on a fraud rule'
    ],
    '320' => [
      'definition'  => 'Gateway Decline - Chargeback',
      'description' => 'The transaction was declined because the previous transaction was charged back'
    ],
    '321' => [
      'definition'  => 'Gateway Decline - Stop Fraud',
      'description' => 'The transaction was declined because the customer record has been flagged as "stop_fraud"'
    ],
    '322' => [
      'definition'  => 'Gateway Decline - Closed Contact',
      'description' => 'The transaction was declined because the customer record has been flagged as "closed_contact"'
    ],
    '323' => [
      'definition'  => 'Gateway Decline - Stop Recurring',
      'description' => 'The transaction was declined because the customer record has been flagged as "stop_recurring"'
    ],
    '400' => [
      'definition'  => 'Transaction error returned by processor',
      'description' => 'Generic error returned from the processor'
    ],
    '410' => [
      'definition'  => 'Invalid merchant configuration',
      'description' => 'Configuration error returned from the processor'
    ],
    '421' => [
      'definition'  => 'Communication error with processor',
      'description' => 'Processor is unreachable'
    ],
    '430' => [
      'definition'  => 'Duplicate transaction at processor',
      'description' => 'Processor is seeing this as a duplicate transaction'
    ],
    '440' => [
      'definition'  => 'Processor Format error',
      'description' => 'Processor has indicated that there is a formating error'
    ],
  ];

  /**
   * @param int|string $code
   *
   * @return mixed
   */
  protected function getResponseByCode($code): mixed {
    return $this->responseTable[(string) $code] ?? [
      'definition'  => 'Unknown response code',
      'description' => 'Unknown, please contact support for more information.'
    ];
  }
}
