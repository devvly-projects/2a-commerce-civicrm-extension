# p2a_commerce

2ACommerce Payment processor for CiviCRM was built to support single and recurring payments, mostly donations.

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP v7.4+
* CiviCRM v5+
* Drupal 8+

## Installation (CLI, Zip)

* Download the archive with module sources and place its contents into `web/sites/default/files/civicrm/ext`
directory.
* Go to Administer - System settings - Extensions and enable the `2A Commerce extension`

## Getting Started

* After the extension was enabled navigate to the Administer - System settings - Payment processor section
* Click the button Add Payment Processor and fill in the form with merchant credentials and API keys
  ![Payment processor creation](images/payment_processor_creation.png)
* Now this payment processor can be used in the Contribution page. It can be added in the Contributions - Manage Contributions pages section with the Add Contribution page button
* And don't forget to enable 2A Commerce payment processor in the Amounts section of a new Contribution page
  ![Binding payment method to the contribution page](images/contribution_page_amounts.png)
