<?php

use Civi\Test\GuzzleTestTrait;

trait CRM_Core_Payment_2ACommerceTrait {
  use GuzzleTestTrait;

  /** @var CRM_Core_Payment $processor */
  protected CRM_Core_Payment $processor;

  /**
   * Is this a recurring transaction
   *
   * @var bool
   */
  protected bool $isRecur = FALSE;

  public function getExpectedSinglePaymentRequests(): array {
    return [];
  }

  public function getExpectedAcquireTokenResponse(): GuzzleHttp\Psr7\Response {
    return new GuzzleHttp\Psr7\Response(200, [], '{"status":"success","msg":"success","data":{"token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NjEyODQzNzEsImlhdCI6MTY2MTE5Nzk3MSwianRpIjoiNDE0ZjIxNDktOTc4ZC00OTgzLWI1ZGMtZDljNTdlYjM1M2RhIiwibWZhIjpmYWxzZSwibmFtZSI6Ikp1c3RpbiBQaGVsYW4iLCJuYmYiOjE2NjExOTc5NzEsInJvbGUiOiJhZG1pbiIsInNpZCI6IjY4NTBlNjRiLWIyMDAtNDBmMy1iNmJjLTAwNDBmN2NiM2E2OSIsInRpZCI6ImMwYzNzdDllcnR0cm0zNGs2bTVnIiwidWlkIjoiYzBjM3N0OWVydHRybTM0azZtNjAiLCJ1c2VybmFtZSI6InRlc3RndW5zdG9yZSIsInV0IjoibWVyY2hhbnQifQ.ZgVhhzpenWc889mcljuRYwHIMB_PLyx3gTR_FRYeXPI","sid":"6850e64b-b200-40f3-b6bc-0040f7cb3a69","ip":"77.123.74.25"}}', '1.1', 'OK');
  }

  public function getExpectedSinglePaymentResponses(): array {
    return [];
  }

  public function getExpectedRecurResponses(): array {
    return [];
  }

  protected function setupMockHandler($id = NULL, $error = FALSE, $recurring = FALSE): void {
    if($id) {
      $this->processor = Civi\Payment\System::singleton()->getById($id);
    }
    $responses = $error ? $this->getExpectedSinglePaymentResponses() : ($recurring ? $this->getExpectedRecurResponses() : $this->getExpectedSinglePaymentResponses());

    $this->createMockHandler($responses);
    $this->setUpClientWithHistoryContainer();
    $this->processor->setGuzzleClient($this->getGuzzleClient());
  }
}
