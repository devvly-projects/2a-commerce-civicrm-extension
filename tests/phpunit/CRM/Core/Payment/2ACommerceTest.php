<?php

use Civi\Test\Api3TestTrait;
use Civi\Test\GuzzleTestTrait;
use CRM_P2aCommerce_ExtensionUtil as E;
use Civi\Test\HeadlessInterface;
use Civi\Test\HookInterface;
use Civi\Test\TransactionalInterface;
use Civi\Test\CiviEnvBuilder;
use PHPUnit\Framework\TestCase;

/**
 * 2ACommerce test suite.
 *
 * Tips:
 *  - With HookInterface, you may implement CiviCRM hooks directly in the test class.
 *    Simply create corresponding functions (e.g. "hook_civicrm_post(...)" or similar).
 *  - With TransactionalInterface, any data changes made by setUp() or test****() functions will
 *    rollback automatically -- as long as you don't manipulate schema or truncate tables.
 *    If this test needs to manipulate schema or truncate tables, then either:
 *       a. Do all that using setupHeadless() and Civi\Test.
 *       b. Disable TransactionalInterface, and handle all setup/teardown yourself.
 *
 * @group headless
 */
class CRM_Core_Payment_2ACommerceTest extends TestCase implements HeadlessInterface, HookInterface, TransactionalInterface {
  use GuzzleTestTrait;
  use Api3TestTrait;
  use CRM_Core_Payment_2ACommerceTrait;
  use CRM_Core_Payment_RecurringHelperTrait;

  /**
   * Setup used when HeadlessInterface is implemented.
   *
   * Civi\Test has many helpers, like install(), uninstall(), sql(), and sqlFile().
   *
   * @link https://github.com/civicrm/org.civicrm.testapalooza/blob/master/civi-test.md
   *
   * @return \Civi\Test\CiviEnvBuilder
   *
   * @throws \CRM_Extension_Exception_ParseException
   */
  public function setUpHeadless(): CiviEnvBuilder {
    return \Civi\Test::headless()
      ->installMe(__DIR__)
      ->apply();
  }

  public function setUp():void {
    parent::setUp();

    $this->paymentProcessorCreate();

    $this->processor = Civi\Payment\System::singleton()
      ->getById($this->ids['PaymentProcessor']['2A Commerce']);
  }

  public function tearDown():void {
    parent::tearDown();
  }

  public function paymentProcessorCreate(): void {
    $paymentProcessorType = $this->callAPISuccess('PaymentProcessorType', 'get', ['name' => '2A Commerce']);
    $this->callAPISuccess('PaymentProcessorType', 'create', ['id' => $paymentProcessorType['id'], 'is_active' => 1]);
    $params = [
      'name' => 'demo',
      'domain_id' => CRM_Core_Config::domainID(),
      'payment_processor_type_id' => '2A Commerce',
      'is_active' => 1,
      'is_default' => 1,
      'is_test' => 1,
      'user_name' => $_ENV['2ACOMMERCE_USER_NAME'],
      'password' => $_ENV['2ACOMMERCE_PASSWORD'],
      'url_site' => 'https://sandbox.fluidpay.com/',
      'class_name' => 'Payment_2ACommerce',
      'billing_mode' => 3,
      'financial_type_id' => 1,
      'financial_account_id' => 12,
      'payment_instrument_id' => 'Credit Card',
    ];
    if (!is_numeric($params['payment_processor_type_id'])) {
      // really the api should handle this through getoptions but it's not exactly api call so lets just sort it
      //here
      $params['payment_processor_type_id'] = $this->callAPISuccess('payment_processor_type', 'getvalue', [
        'name' => $params['payment_processor_type_id'],
        'return' => 'id',
      ], 'integer');
    }
    $result = $this->callAPISuccess('payment_processor', 'create', $params);
    $processorID = $result['id'];
    //$this->setupMockHandler($processorID);
    $this->ids['PaymentProcessor']['2A Commerce'] = $processorID;
  }

  /**
   * Example: Test that a version is returned.
   */
  public function testWellFormedVersion():void {
    $this->assertNotEmpty(E::SHORT_NAME);
    $this->assertMatchesRegularExpression('/^([0-9\.]|alpha|beta)*$/', \CRM_Utils_System::version());
  }

  /**
   * Example: Test that we're using a fake CMS.
   */
  public function testWellFormedUF():void {
    $this->assertEquals('UnitTests', CIVICRM_UF);
  }

  /**
   * @covers \CRM_Core_Payment_RecurringHelperTrait::getBillingFrequency()
   */
  public function testBillingFrequency() {
    $this->assertEquals('monthly', $this->getBillingFrequency('month'));
    $this->assertEquals('monthly', $this->getBillingFrequency('year'));
    $this->assertEquals('daily', $this->getBillingFrequency());
  }

  /**
   * @covers \CRM_Core_Payment_RecurringHelperTrait::getBillingCycleInterval()
   */
  public function testBillingCycleInterval() {
    $this->assertEquals(1, $this->getBillingCycleInterval('month'));
    $this->assertEquals(1, $this->getBillingCycleInterval('day'));
    $this->assertEquals(12, $this->getBillingCycleInterval('year'));
  }

  /**
   * @covers \CRM_Core_Payment_RecurringHelperTrait::getBillingDays()
   */
  public function testBillingDays() {
    $this->assertEquals('1', $this->getBillingDays('day'));
    $this->assertEquals(date('d'), $this->getBillingDays('year'));
    $this->assertEquals('0', $this->getBillingDays('month'));
  }

  public function testNextBillingDate() {
    $this->assertEquals(
      date('Y-m-d', strtotime('now +1 day')),
      $this->getNextBillingDate('day')
    );

    $this->assertEquals(
      date('Y-m-d', strtotime('now +1 month')),
      $this->getNextBillingDate('month')
    );

    $this->assertEquals(
      date('Y-m-d', strtotime('now +12 months')),
      $this->getNextBillingDate('year')
    );

  }

}
